namespace MichalHudek
{
    public interface IInteractable
    {
        void Interact();
    }
}
