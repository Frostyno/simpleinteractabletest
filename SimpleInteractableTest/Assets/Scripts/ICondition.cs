namespace MichalHudek
{
    public interface ICondition
    {
        bool IsMet();
    }
}
