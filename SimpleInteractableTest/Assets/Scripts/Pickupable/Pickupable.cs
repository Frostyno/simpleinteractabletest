using System;
using UnityEngine;

namespace MichalHudek
{
    public class Pickupable : MonoBehaviour
    {
        Transform _owner = null;

        public bool IsPickedUp => _owner != null;
        
        void Update()
        {
            if (_owner == null) return;
            transform.position = _owner.position;
            transform.rotation = _owner.rotation;
        }

        public void PickUp(Transform owner) => _owner = owner;
        public void Drop() => _owner = null;
    }
}
