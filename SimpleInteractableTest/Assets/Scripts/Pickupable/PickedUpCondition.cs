using UnityEngine;

namespace MichalHudek
{
    public class PickedUpCondition : MonoBehaviour, ICondition
    {
        [SerializeField] Pickupable _pickupable = null;
        
        public bool IsMet() => _pickupable != null && _pickupable.IsPickedUp;
    }
}
