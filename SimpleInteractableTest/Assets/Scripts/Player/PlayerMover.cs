using UnityEngine;

namespace MichalHudek
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] InputHandler _inputHandler = null;
        [SerializeField] float _movementSpeed = 4f;
        
        Rigidbody2D _rigidbody;
        Camera _camera;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _camera = Camera.main;
        }

        void FixedUpdate()
        {
            _rigidbody.velocity = _movementSpeed * _inputHandler.MovementInput.normalized;

            RotateToMouse();
        }

        void RotateToMouse()
        {
            var directionVector = _inputHandler.MousePositionWorld - (Vector2)transform.position;
            var angle = Vector2.Angle(Vector2.up, directionVector.normalized);
            angle *= -Mathf.Sign(directionVector.x);
            transform.eulerAngles = transform.eulerAngles.With(z: angle);
        }
    }
}
