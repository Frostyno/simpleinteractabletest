using System;
using UnityEngine;

namespace MichalHudek
{
    public class ItemPicker : MonoBehaviour
    {
        [SerializeField] Transform _handPivot = null;
        [SerializeField] InputHandler _inputHandler = null;

        Pickupable _pickupableInRange = null;

        public Pickupable HeldItem { get; private set; } = null;

        public event Action OnItemDropped = null;
        public event Action OnItemPickedUp = null;

        void Awake()
        {
            _inputHandler.OnPickUp += ProcessPickup;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.TryGetComponent(out Pickupable pickupable))
                 _pickupableInRange = pickupable;
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (_pickupableInRange == null) return;
            if (other.gameObject == _pickupableInRange.gameObject)
                _pickupableInRange = null;
        }
        
        void ProcessPickup()
        {
            if (!Input.GetButtonDown("Fire2")) return;

            if (HeldItem != null)
            {
                HeldItem.Drop();
                OnItemDropped?.Invoke();
                HeldItem = null;
            }

            if (_pickupableInRange == null) return;
            HeldItem = _pickupableInRange;
            HeldItem.PickUp(_handPivot);
            _pickupableInRange = null;
            OnItemPickedUp?.Invoke();
        }
    }
}
