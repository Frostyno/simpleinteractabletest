using UnityEngine;

namespace MichalHudek
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] InputHandler _inputHandler = null;
        [SerializeField] ItemPicker _itemPicker = null;
        [SerializeField] InteractableDetector _interactableDetector = null;
        
        IInteractable _usedInteractable = null;

        void Awake()
        {
            _itemPicker.OnItemDropped += EndInteractionWithHeldItem;
            _interactableDetector.OnInteractableLost += EndInteractionWithGroundedItem;

            _inputHandler.OnInteract += Interact;
            _inputHandler.OnInteractEnd += EndInteraction;
        }

        void EndInteractionWithGroundedItem()
        {
            if (_usedInteractable == _interactableDetector.InteractableInRange)
                EndInteraction();
        }

        void EndInteractionWithHeldItem()
        {
            if (_usedInteractable == _itemPicker.HeldItem.GetComponent<IInteractable>())
                EndInteraction();
        }

        void Interact()
        {
            _usedInteractable = GetUsableInteractable();
            _usedInteractable?.Interact();
        }
        
        void EndInteraction()
        {
            if (_usedInteractable is IInteractEndable endable)
                endable.EndInteraction();

            _usedInteractable = null;
        }

        IInteractable GetUsableInteractable()
        {
            IInteractable interactable = null;
            if (_itemPicker.HeldItem != null)
                interactable = _itemPicker.HeldItem.GetComponent<IInteractable>();
            
            return interactable ?? _interactableDetector.InteractableInRange;
        }
    }
}
