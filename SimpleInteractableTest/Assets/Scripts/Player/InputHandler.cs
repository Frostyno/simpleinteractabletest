using System;
using UnityEngine;

namespace MichalHudek
{
    public class InputHandler : MonoBehaviour
    {
        Camera _camera;
        
        public Vector2 MovementInput { get; private set; } = Vector2.zero;
        public Vector2 MousePosition => Input.mousePosition;
        public Vector2 MousePositionWorld => _camera.ScreenToWorldPoint(MousePosition);

        public event Action OnInteract = null;
        public event Action OnInteractEnd = null;
        public event Action OnPickUp = null;

        void Awake() => _camera = Camera.main;

        void Update()
        {
            MovementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            
            if (Input.GetButtonDown("Fire1"))
                OnInteract?.Invoke();
            else if (Input.GetButtonUp("Fire1"))
                OnInteractEnd?.Invoke();;
            
            if (Input.GetButtonDown("Fire2"))
                OnPickUp?.Invoke();
        }
    }
}
