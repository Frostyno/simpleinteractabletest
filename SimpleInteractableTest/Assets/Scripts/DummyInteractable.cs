using UnityEngine;

namespace MichalHudek
{
    public class DummyInteractable : MonoBehaviour, IInteractable
    {
        public void Interact()
        {
            Debug.Log("<color=yellow>Interacted with DummyInteractable.</color>");
        }
    }
}
