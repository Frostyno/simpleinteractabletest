using System;
using UnityEngine;

namespace MichalHudek
{
    public class InteractableDetector : MonoBehaviour
    {
        public IInteractable InteractableInRange { get; private set; } = null;

        public event Action OnInteractableLost = null;
        
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.TryGetComponent(out IInteractable interactable))
                InteractableInRange = interactable;
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<IInteractable>() != InteractableInRange)
                return;
            
            OnInteractableLost?.Invoke();
            InteractableInRange = null;
        }
    }
}
