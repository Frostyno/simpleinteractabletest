namespace MichalHudek
{
    public interface IInteractEndable
    {
        void EndInteraction();
    }
}