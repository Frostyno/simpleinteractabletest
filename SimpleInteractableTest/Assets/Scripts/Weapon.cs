using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MichalHudek
{
    public class Weapon : MonoBehaviour, IInteractable, IInteractEndable
    {
        [SerializeField] float _attackRate = 1f;

        Timer _attackTimer;
        List<ICondition> _interactionConditions;
        
        void Awake()
        {
            _interactionConditions = GetComponents<ICondition>().ToList();
            
            _attackTimer = new NonSpamableTimer(_attackRate)
            {
                Repeat = true,
                InvokeOnStart = true
            };
            _attackTimer.OnTimeout += SpawnProjectile;
        }

        void Update()
        {
            _attackTimer.Tick(Time.deltaTime);
        }

        public void Interact()
        {
            if (_interactionConditions.Any(c => !c.IsMet()))
                return;
            
            _attackTimer.Start();
        }

        public void EndInteraction()
        {
            _attackTimer.Stop();
        }

        void SpawnProjectile()
        {
            Debug.Log($"<color=#ff0000>Projectile spawned</color> - {name}");
        }
    }
}
