using System;

namespace MichalHudek
{
    public class Timer
    {
        protected float Elapsed { get; private set; } = 0f;
        protected float Interval { get; private set; }
        protected bool Running { get; private set; } = false;
        public bool Repeat { get; set; } = false;
        public bool InvokeOnStart { get; set; } = false;

        public event Action OnTimeout = null;

        public Timer(float interval) => Interval = interval;

        public virtual void Start()
        {
            if (Running) return;
            
            Reset();
            Running = true;
            
            if (InvokeOnStart)
                OnTimeout?.Invoke();
        }

        public virtual void Stop()
        {
            Running = false;
        }
        
        public virtual void Tick(float deltaTime)
        {
            if (!Running) return;

            Elapsed += deltaTime;
            if (Elapsed < Interval) return;
            
            OnTimeout?.Invoke();
            if (Repeat)
                Reset();
            else
                Stop();
        }

        void Reset() => Elapsed = 0f;
    }
}
