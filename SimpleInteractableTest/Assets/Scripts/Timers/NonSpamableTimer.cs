namespace MichalHudek
{
    public class NonSpamableTimer : Timer
    {
        float _cooldownTime = 0f;
        bool _startAfterCooldown = false;
        
        public NonSpamableTimer(float interval) : base(interval)
        {
        }

        public override void Start()
        {
            if (_cooldownTime > 0f)
            {
                _startAfterCooldown = true;
                return;
            }
            
            base.Start();
        }

        public override void Stop()
        {
            if (Running && (Elapsed < Interval))
                _cooldownTime = Interval - Elapsed;
            
            _startAfterCooldown = false;
            
            base.Stop();
        }

        public override void Tick(float deltaTime)
        {
            if (_cooldownTime > 0f)
                _cooldownTime -= deltaTime;
            else if (_startAfterCooldown)
            {
                _startAfterCooldown = false;
                Start();
            }
            
            base.Tick(deltaTime);
        }
    }
}
